## MICROFRONTED

### Clonar aplicación
````
$ git clone https://gitlab.com/Edwin73/microfrontend.git
$ git submodule init
$ git submodule update
$ cd frontend
$ cd microfronted-items
$ npm i
$ npm run dev
$ cd ../microfronted-auth
$ npm i
$ npm run dev
$ cd ../microfronted-container
$ npm i
$ npm run dev

````

Levantar la aplicación en el `http://localhost:3000`

### ¿Que es?

Micro Frontend es uno de los temas más candentes en Internet en este momento. Lo escuchamos todo el tiempo, pero ¿qué es micro Frontend? Imaginate una aplicación web con varios modulos `carrito de compras`, `dashboard`, `lista de articulos`,`login`, `header`, `footer` entre otros; desarrollar y dar mantenimiento se complicaria a medida que la aplicación va creciendo. Ahi es donde entra los microfrontend, ya que nos permitiria separar cada módulo en varias aplicaciones independientes, con lo cual un equipo de desarrollo podria enfocarse en un solo modulo y trabajarlo de manera independiente, además de que nos permite desarollarlo en distintos frameworks, por ejemplo el carrito de compras en React y el dashboard en Angular.

Gracias a eso cada equipo se encarga de una o más funcionalidades; la idea es que las funcionalidades de cada equipo tengan pocas dependencias con las de los otros equipos para que esta composición sea lo más fácil posible.

La principal novedad de los micro frontends es que los equipos de desarrollo no se separan por tecnología (el clásico ejemplo de equipos backend y equipos frontend) sino que són equipos transversales con todos los perfiles necesarios para desarrollar una funcionalidad completa.

### Pros y Contras

##### Pros
 * Simplifica las actualizaciones
 * Desarrollo independiente de cada módulo
 * Mayor escalabilidad de las aplicaciones
 * Generar código mas pequeño y más manejable
 * Actualizar o incluso reescribir partes de la interfaz de forma más fluida que las SPA.

##### Contras
* Pruebas complejas
* El proceso de configuración y despliegue para cada frontend es diferente

### Esquema

![arqui](https://www.cygnismedia.com/images/post-images/micro-front-ends-development/what-is-micro-frontends.jpg)

### Build-Time Integrations

El contenedor actúa como una aplicación independiente, y los microfrontend son solo dependencias que se utilizan dentro.

#### Ventajas
 * Facil de configurar y entender
#### Desventajas
 * El contenedor debe ser desplegado cada vez que existe un cambio en uno de los frontend
 

#### Run-Time Integrations

El contenedor no contendrá automáticamente todos los micro-frontends, y el navegador del usuario no las descargará cada vez que se solicite el punto de entrada de la aplicación, sino solo cuando se alcance la ruta

#### Ventajas
 * Los frontends pueden ser despleagos de manera independiente
 * Pueden exister varias verisones de los front desplegados, y el contenedor va a poder desidir cual de esas usar
#### Desventajas
* La configuración tiene su complejidad

#### Module Federation

*****

## Ejemplo práctico

#### Poyecto
Se creará una pequeña aplicación en React, haciendo uso del plugin de **Module Federation**, el cual actualmente es parte de Webpack5.

Gracias eso podremos separar nuestro aplicación en 3 frontends independientes:
 * Container => Sera el contenedor principal, donde se alojara los distintos frontends.
 * ItemsApp => Aplicación que mostrará un listado de artículos
 * AuthApp => Aplicación que permitira loguearse a un usuario

#### Esquema del Proyecto

![image-5.png](./image-5.png)

#### Dependencias

````
    "clean-webpack-plugin": "^3.0.0",
    "webpack": "^5.4.0",
    "webpack-cli": "^4.1.0",
    "webpack-dev-server": "^3.11.0",
    "webpack-merge": "^5.2.0"
````


#### Estructura del Proyecto
 * Contenedor

 ![image-3.png](./image-3.png)

 * Items y Auth app

 ![image-4.png](./image-4.png)


### 

 ### Configuración Items App
 * Creamos un archivo main.js y configuramos el arranque de la aplicación de la siguiente manera:

````
import React from 'react';
import ReactDom from 'react-dom'
import App from './App';

// montar funcion para iniciar la app
const ItemsReactComponent = (element) => {
    ReactDom.render(
        <App/>,
        element,
    );
}

// llamar al mount si estamos ejecutando en dev y/o aislamiento
if (process.env.NODE_ENV === 'development') {
    // obtenemos el id del html.index
    const devRoot = document.querySelector('#__items_app');
    if (devRoot) {
        ItemsReactComponent(devRoot);
    }
}

// exportamos la funcion para montar la app en el contenedor
export {ItemsReactComponent};

```` 

 * Borramos el contenido de `index.js` e importamos el archivo main.js, de esta forma estamos cargando de forma asincrona
```
import('./main')
```


* Nos dirigimos al directorio `config/webpack.config.js`, y realizamos la configuración que se muestra a continuación, haciendo uso del plugin **Module Federation**

````
const {merge} = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');
const packageJson = require('../package.json')

const devConfig = {
    mode: 'development',
    devServer: {
        port: 3002,
        historyApiFallback: {
            index: 'index.html'
        },
    },
    plugins: [
        new ModuleFederationPlugin({
            name: 'items',
            filename: 'remoteEntry.js',
            exposes: {
                './ItemsApp': './src/main',
            },
            shared: packageJson.dependencies
        }),
        new HtmlWebpackPlugin({
            template: './public/index.html',
        }),
    ]
}

module.exports = merge(commonConfig, devConfig);
````

Los puntos a resaltar de esta configuración son:
  * `name`: se utilizará como nombre de archivo de entrada.
  * `filename`: el nombre del archivo de entrada especializado.
  * `exposes`: expone el componente para que lo consuman otras aplicaciones.
  * `shared`: esto importará las dependencias, si la aplicación que las consume consumidores no la tiene.

No olvidar los nombres colocados en los campos `name` y `filename`, ya que estos campos seran requeridos en la configuración del contenedor.


* Ahora dentro del `package.json` colocamos en la sección de scripts la configuración para arrancar nuestra aplicación
````
  "scripts": {
    "dev": "webpack serve --config config/webpack.dev.js"
  },
````

* Realizamos los pasos similares para configurar AuthApp

****

### Configuración ContainerApp

 * Creamos un archivo main.js, el cual sera el encargado de arrancar nuestra aplicación

 ```
 import React from 'react';
import ReactDom from 'react-dom'

import App from './App';

// montar funcion para iniciar la app
ReactDom.render(
    <App/>,
    document.querySelector('#__container'),
);
 ```

 * Borramos el contenido de `index.js` e importamos el archivo main.js, de esta forma estamos cargando de forma asincrona
```
import('./main')
```

* Nos dirigimos al directorio `config/webpack.config.js`, y realizamos la configuración que se muestra a continuación

```
const {merge} = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');
const packageJson = require('../package.json')

const devConfig = {
    mode: 'development',
    output: {
        publicPath: 'http://localhost:3000/',
    },
    devServer: {
        port: 3000,
        historyApiFallback: {
            index: 'index.html'
        },
    },
    plugins: [
        new ModuleFederationPlugin({
            name: 'container',
            remotes: {
                auth: 'auth@http://localhost:3001/remoteEntry.js',
                items: 'items@http://localhost:3002/remoteEntry.js',
            },
            shared: packageJson.dependencies
        }),
        new HtmlWebpackPlugin({
            template: './public/index.html',
        }),
    ]
}

module.exports = merge(commonConfig, devConfig);
```


Notamos que a diferencia de los frontends hijos, el contendor dentro del plugin Module Federation no maneja la propuedad `exposes`, sino que por el contrario se hace uso de `remotes`:

```
            remotes: {
                auth: 'auth@http://localhost:3001/remoteEntry.js',
                items: 'items@http://localhost:3002/remoteEntry.js',
            },
```


En esta sección tomar en cuenta que los nombres coincidan con los colocados en la propiedad `name` de los frotends Items y Auth.


* Ahora actualizamos el `package.json`, colocamos en la sección de scripts la configuración para arrancar nuestra aplicación
````
  "scripts": {
    "dev": "webpack serve --config config/webpack.dev.js"
  },
````

* Creamos un nuevo archivo Auth.js e importamos el Frontend ItemApp dentro hacia el contendor
```
import React, {useRef, useEffect} from "react"
import {AuthReactComponent} from 'auth/AuthApp'


const Auth = () => {
    const ref = useRef(null)

    useEffect(
        () => {
            AuthReactComponent(ref.current)
        }, []
    )

    return <div ref={ref}/>
}

export default Auth

```
Se debe tener en cuenta que el `AuthReactComponent` es el mismo que exportamos en el `main.js` del microfrontend Items, y la ruta de donde se la importa es:

* `items` -> el nombre que se colocó dentro del Module Federation Plugin, en la sección **remotes** del WebpackConfig del Contenedor, y además que dicho nombre debe coincidir con el atributo `name` que se encuentra dentro del Module Federation Plugin del WebpackConfig del ItemsApp.
* `AuthApp` ->es la llave que se encuentra dentro de Module Federation Plugin, en la sección `exposes` del WebpackConfig del Contenedor


* De igual manera crear un archivo Items.js e importamos el frontend en el contendor:

```
import React, {useRef, useEffect} from "react";
import {ItemsReactComponent} from 'items/ItemsApp'


const Items = () => {
    const ref = useRef(null)

    useEffect(
        () => {
            ItemsReactComponent(ref.current)
        }, []
    )

    return <div ref={ref}/>
}

export default Items

```

* Y finalmente los colocamos dentro de una página, para su posterior uso dentro de la navegación

```
import React, {useState} from "react";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";

import Items from "./components/Items";
import Auth from "./components/Auth";
import Header from "./components/Header";


const App = () => {
    const [] = useState()
    return (
        <>
            <h3>Container app!!!</h3>
            <BrowserRouter>
                <Header/>
                <div>
                    <Switch>
                        <Route path="/auth" component={Auth} />
                        <Route path="/" component={Items} />
                        <Redirect to="/"/>
                    </Switch>
                </div>
            </BrowserRouter>
        </>
    )
}

export default App

```

* Levantamos las aplicaciones de manera independiente:


